#! /usr/bin/env ruby
#
#  encoding: UTF-8
#   check-port-hosts
#
# DESCRIPTION:
# Connect to a TCP/UDP port on one or more hosts to see if open.   Don't use nmap since it's overkill.
#
# OUTPUT:
#   plain text
#
# PLATFORMS:
#   Linux
#
# DEPENDENCIES:
#   gem: sensu-plugin
#
# USAGE:
#
# Ports are comma separated and support ranges
# ./check-port-hosts.rb -H 192.168.1.1-3 -p 22 -P tcp -W 3 -C 0
#
# NOTES:
# By default, checks for openssh on localhost port 22
#
#
# LICENSE:
#   Released under the same terms as Sensu (the MIT license); see LICENSE
#   for details.

require 'sensu-plugin/check/cli'
require 'socket'
require 'timeout'
require 'thread'
require 'ipaddr'

#
# Check Banner
#
class CheckPortHost < Sensu::Plugin::Check::CLI
  option :hosts,
         short: '-H HOSTNAMES',
         long: '--hostnames HOSTNAMES',
         description: 'Hosts to connect to, comma seperated, can use ranges in last octet',
         default: '0.0.0.0'

  option :port,
         short: '-p PORT',
         long: '--port PORT',
         description: 'Port to check',
         default: '22'

  option :proto,
         short: '-P PROTOCOL',
         long: '--protocol PROTOCOL',
         description: 'Protocol to check: tcp (default) or udp',
         default: 'tcp'

  option :timeout,
         short: '-t SECS',
         long: '--timeout SECS',
         description: 'Connection timeout',
         proc: proc(&:to_i),
         default: 30
         
  option :warn_count,
         short: '-W NUMBER',
         description: 'Warn if success less than or equal to NUMBER',
         proc: proc(&:to_i),
         default: 0

  option :critical_count,
         short: '-C NUMBER',
         description: 'Critical if success less than or equal to NUMBER',
         proc: proc(&:to_i),
         default: 0
         
  option :refuse_is_success,
         short: '-r',
         long: '--refuse-is-success',
         boolean: true,
         description: 'If connection is refused, we consider it a successful contact',
         default: false

  def check_port(port, host)
    timeout(config[:timeout]) do
      config[:proto].casecmp('tcp').zero? ? TCPSocket.new(host, port.to_i) : UDPSocket.open.connect(host, port.to_i)
    end
  rescue Errno::ECONNREFUSED
    config[:refuse_is_success]
  rescue Timeout::Error, Errno::EHOSTUNREACH, EOFError
    false
  end

  def run
    port = config[:port]
    hosts = config[:hosts].split(',').flat_map do |host|
      # Host range in last IP octet
      hostmatch = host.match(/\A(\d{1,3}\.\d{1,3}\.\d{1,3}\.)(\d{1,3}-\d{1,3})\Z/)
      if hostmatch
        prefix = hostmatch[1]
        suffix = hostmatch[2]
        first_host, last_host = suffix.split('-')
        (first_host.to_i..last_host.to_i).map {|octet| prefix + octet.to_s}
      # Single host
      else
        host
      end
    end
    
    okqueue = Queue.new
    failqueue = Queue.new
    threads = []
    hosts.each do |host|
      threads << Thread.new(host) do |th|
        (check_port(port, host) ? okqueue : failqueue) << host
      end
    end
    threads.each{|th| th.join}
    
    downhosts = failqueue.size.times.map{ failqueue.pop }.sort{ |a,b| IPAddr.new( a ) <=> IPAddr.new( b ) } .join(', ')
    
    if okqueue.size == hosts.size
      ok "All hosts (#{config[:hosts]}) have port #{config[:port]} accessible"
    elsif okqueue.size <= config[:critical_count]
      critical "Only #{okqueue.size}/#{hosts.size} (<= #{config[:critical_count]}) hosts have port #{config[:port]} accessible (down: #{downhosts})"
    elsif okqueue.size <= config[:warn_count]
      warning "Only #{okqueue.size}/#{hosts.size} (<= #{config[:warn_count]}) hosts have port #{config[:port]} accessible (down: #{downhosts})"
    end
  end
end
